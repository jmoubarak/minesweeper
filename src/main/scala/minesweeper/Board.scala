package minesweeper
import scala.util.Random

class Board(val matrix:Matrix[Cell]){

    def apply(i:Int, j:Int):Cell=matrix(i)(j)

    def print():Unit={
        matrix.mapRows(row=>row.map(_.print).mkString(" ")).foreach(println)
    }

}

object Board{

    def generate(height:Int, width:Int, nMines:Int):Board={
        require(height > 0, "height should be strictly positive")
        require(width > 0, "width should be strictly positive")
        require(nMines > 0 && nMines <= height * width, "mines should be strictly positive and no more than height*width")
        val matrix = Matrix(height, width)(new Cell.Builder(false,0))
        val minesLoc = Random.shuffle((0 until height).flatMap(i=>(0 until width).map(j=>(i,j)))).take(nMines)

        minesLoc.foreach{case(i,j)=> 
            matrix(i)(j)=Cell(true,0)
            matrix.neighboursOf(i,j).foreach(_.number+=1)
        }

        new Board(matrix.mapCells(cell=>cell.build))
    }

}