package minesweeper

case class Cell private (hasMine:Boolean, number:Int){
    var flagged:Boolean = false
    var covered:Boolean = true

    def print:String ={
        if(hasMine) {
            Console.RED_B + "x" + Console.RESET
        } else{
            number match{
                case 0 => " " 
                case 1 => color(Console.BLUE)
                case 2 => color(Console.GREEN)
                case 3 => color(Console.RED)
                case 4 => color(Console.MAGENTA)
                case 5 => color(Console.YELLOW)
                case 6 => color(Console.CYAN)
                case 7 => color(Console.YELLOW)
                case 8 => color(Console.MAGENTA)
                case n => n.toString
            }
        }
    }

    private def color(color:String):String={
        color + number.toString + Console.RESET
    }
}

object Cell{
    class Builder(hasMine:Boolean, var number:Int){
        def build = new Cell(hasMine, number)
    }
    def apply(hasMine:Boolean, number:Int)=new Builder(hasMine, number)
}