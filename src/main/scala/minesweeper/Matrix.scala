package minesweeper

import scala.reflect.ClassTag

class Matrix[T:ClassTag](private[minesweeper] val matrix:Array[Array[T]]){

    val height = matrix.length
    val width = matrix.head.length

    def apply(i:Int,j:Int):T=matrix(i)(j)
    def apply(i:Int):Array[T]=matrix(i)

    def neighboursOf(i:Int, j:Int):Seq[T] = {
        Seq((i,j-1), (i,j+1), (i-1,j), (i+1,j), (i-1,j-1), (i-1,j+1), (i+1,j-1), (i+1,j+1))
            .filter{ case(i,j) => i>=0 && i<height && j>=0 && j < width}
            .map{ case(i,j) => matrix(i)(j)}
    }

    def mapCells[R:ClassTag](f: T=>R):Matrix[R] = {
        val newMatrix = matrix.map(_.map(f).toArray)
        new Matrix(newMatrix)
    }

    def mapRows[R:ClassTag](f: Array[T]=>R):Array[R]={
        matrix.map(f)
    }

    
}

object Matrix{
    def apply[T:ClassTag](height:Int, width:Int)(f: =>T)={
        val array=Array.fill[T](height,width)(f)
        new Matrix(array)
    }

    def apply[T:ClassTag](array:Array[Array[T]])={
        new Matrix(array)
    }
}