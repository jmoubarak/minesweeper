package minesweeper

import org.scalatest._
import org.scalatest.prop.TableDrivenPropertyChecks._

class MatrixSpec extends FlatSpec with Matchers with GivenWhenThen {

    "Matrix" should "set dimensions" in {
        val array = Array.fill(4,5)(0)
        When("using constructor")
        val matrix1 = new Matrix(array)
        matrix1.height shouldBe 4
        matrix1.matrix.length shouldBe 4
        matrix1.width shouldBe 5
        matrix1.matrix.foreach(_.length shouldBe 5)
        When("using companion object")
        val matrix2 = Matrix(3,6)(0)
        matrix2.height shouldBe 3
        matrix2.matrix.length shouldBe 3
        matrix2.width shouldBe 6
        matrix2.matrix.foreach(_.length shouldBe 6)
        }

    it should "retrieve individual cells" in {
        val array = Array(
            Array(1,2,3),
            Array(4,5,6)
            )
        val matrix = new Matrix(array)
        for{i<-0 to 1
            j<- 0 to 2
        }  yield {
            matrix(i,j) shouldBe array(i)(j)
        }
    }
    it should "retrieve rows" in {
        val array = Array(
            Array(1,2,3),
            Array(4,5,6)
            )
        val matrix = new Matrix(array)
        matrix(0) shouldBe array(0)
        matrix(1) shouldBe array(1)
    }

    it should "map cells" in {
        val array = Array(
            Array(1,2,3),
            Array(4,5,6)
            )
        val matrix = new Matrix(array)
        val mapped = matrix.mapCells(_ * 0.5)
        mapped shouldBe a[Matrix[_]]
         for{i<-0 to 1
            j<- 0 to 2
        }  yield {
            mapped(i,j) shouldBe array(i)(j) * 0.5
        }
    }

    it should "map rows" in {
        val array = Array(
            Array(1,2,3),
            Array(4,5,6)
            )
        val matrix = new Matrix(array)
        val mapped = matrix.mapRows(_.length)
        mapped(0) shouldBe 3
        mapped(1) shouldBe 3
    }

    it should "retrieve neighbour cells" in {
        val array = Array(
            Array(1,2,3),
            Array(4,5,6),
            Array(7,8,9)
            )
        val matrix = new Matrix(array)

        val table = Table(
            ("i","j","neighbours"),
            (0,0,Set(2,4,5)),
            (0,1,Set(1,3,4,5,6)),
            (0,2,Set(2,5,6)),
            (1,0,Set(1,2,5,7,8)),
            (1,1,Set(1,2,3,4,6,7,8,9)),
            (1,2,Set(2,3,5,8,9)),
            (2,0,Set(4,5,8)),
            (2,1,Set(4,5,6,7,9)),
            (2,2,Set(5,6,8))
        )
        forAll(table){(i,j,values)=>
            matrix.neighboursOf(i,j) should contain theSameElementsAs values
        }
    }         
}