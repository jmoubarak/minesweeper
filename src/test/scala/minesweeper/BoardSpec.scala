package minesweeper

import org.scalatest._
import org.scalatest.prop.TableDrivenPropertyChecks._

class BoardSpec extends FlatSpec with Matchers with GivenWhenThen {
  "The Board" should "generate a minesweeper matrix" in {
    Given("a height, a width and a number of mines")
    val height = 10
    val width = 15
    val mines = 10
    When("Generating the board")
    val board = Board.generate(height,width,mines)
    val matrix = board.matrix
    Then("it should have the configured height")
    matrix.height shouldBe height
    And("it should have the configured width")
    matrix.width shouldBe width
    And("it should have the configured number of mines")
    val actualMines = for{
        i<-0 until height
        j<-0 until width
      } yield{
        if(board(i,j).hasMine) 1 else 0
      }
    actualMines.sum shouldBe mines
    noException should be thrownBy board.print()
  }

  it should "check input" in {
      val table = Table(("height", "width","mines"),
      (0,0,0),
      (1,0,0),
      (0,1,0),
      (0,0,1),
      (0,1,1),
      (1,0,1),
      (1,1,0),
      (-1,-1,-1),
      (-1,1,-1),
      (-1,-1,1),
      (-1,1,1),
      (1,-1,1),
      (1,1,-1),
      (1,-1,-1)
      )
      forAll(table){(h,w,m)=>
        intercept[Exception]{
          Board.generate(h,w,m)
        }
      }
  }
}
